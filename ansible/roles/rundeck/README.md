Rundeck
=========

[![pipeline status](https://gitlab.com/Rivendel-Labs/ansible/roles/rundeck/badges/master/pipeline.svg)](https://gitlab.com/Rivendel-Labs/ansible/roles/rundeck/commits/master)

O Rundeck auxilia na automação de procedimentos operacionais de rotina em Datacenters ou ambientes em nuvem.

Requerimentos
------------
Nenhum

Variaveis
--------------

Nenhum

Dependencias
------------

Nenhum

Playbook exemplo
----------------

    - hosts: all
      roles:
         - { role: rundeck }

Licença
-------

BSD

Author Information
------------------

Esta role foi desenvolvida por Thiago Freitas (thiago.freitas@rivendel.com.br) em 03/2018
