# Estrutura Terraform

A estrutura atual utilizada do terraform é composta de 2 tipos distintos de configuracao, uma apenas de modulos reutilizaveis onde qualquer estrutura de pastas e/ou organizacao pode ser usada e outro conjunto de pastas que realiza as chamadas ao modulos e tem sua propria estrutura de pastas, além obviamente de iniciar com o nome do backend a qual aquela estrutura pertence.

Atualmente este repositorio conta com o suporte aos seguintes backends:

- aws (Amazon Web Services)

## Modulos

A pasta `modules` na raiz do projeto se destina aos modulos comumente utilizados no terraform, e segue o seguinte padrao:

    .
    └── modulo
        ├── main.tf
        ├── outputs.tf
        ├── README.md
        └── vars.tf

A estrutura de nomes segue o padrao comumente utilizado por projetos terraform, onde o `main.tf` contem todas as chamadas aos resources, o arquivo `vars.tf` as variaveis possiveis do modulo e seus padroes e o arquivo `outputs.tf` recebe os valores a serem exibidos ao termino da execução do modulo, assim como o armazenamento desses valores no arquivo state.

## Envs / Environments

Esta estrutura de pastas foi criada baseando-se no fato de ser possivel o arquivo `outputs.tf ser incluido como um modulo`, assim conseguindo gerar um conjunto de variaveis com o objetivo de definir alguns valores padroes para aquele ambiente, a estrutura base segue o seguinte padrao:

    .
    └── environment (ex: dev/stg/prd)
        ├── outputs.tf
        ├── data
        │   ├── elasticsearch
        │   │   ├── main.tf
        │   │   ├── outputs.tf
        │   │   └── vars.tf
        │   └── rds
        │       ├── mysql
        │       │   ├── main.tf
        │       │   ├── outputs.tf
        │       │   └── vars.tf
        │       └── postgres
        │           ├── main.tf
        │           ├── outputs.tf
        │           └── vars.tf
        ├── network
        │   ├── vpc
        │   │   ├── main.tf
        │   │   ├── outputs.tf
        │   │   └── vars.tf
        │   └── vpn
        │       ├── main.tf
        │       ├── outputs.tf
        │       └── vars.tf
        └── services
            └── app
                ├── main.tf
                ├── outputs.tf
                ├── user_data.tpl
                └── vars.tf

A estrutura segue o mesmo padrao dos modulos, com a diferenca que os arquivos main.tf, vars.tf e outputs.tf de cada pasta de serviço, esta contido apenas naquela pasta, se limitando apenas realizar chamadas aos modulos de forma independente, veja o exemplo a seguir de uma chamada para criacao de uma VPC:

    provider "aws" {
      region = "${var.region == "" ?  module.environment.region : var.region}"
    }

    terraform {
      backend "consul" {
        path = "network/vpc"
      }
    }

    # Onde o arquivo outputs.tf é carregado como modulo e disponibiliza as variaveis do ambiente.
    module "environment" {
      source = "../../"
    }

    module "keypair" {
      source = "../../../../modules/keypair"

      client_name = "${var.client_name == "" ?  module.environment.client_name : var.client_name}"
      environment = "${var.environment == "" ?  module.environment.environment : var.environment}"
    }

    module "vpc" {
      source = "../../../../modules/vpc"

      zone_name   = "${var.client_name == "" ?  module.environment.client_name : var.client_name}"
      vpc_name    = "${var.vpc_name == "" ?  module.environment.vpc_name : var.vpc_name}"
      environment = "${module.environment.environment}"
      cidr_vpc    = "${module.environment.cidr_vpc}"
    }

Perceba que as variaveis que iniciam com `module.environment` estao com os valores definidos no arquivo `{dev,stg,prd}/outputs.tf`, veja:

    output "client_name" {
      value = ""
    }

    output "environment" {
      value = "dev"
    }

    output "region" {
      value = "us-east-1"
    }

    output "vpc_name" {
      value = "vpc-dev"
    }

    output "cidr_vpc" {
      value = "10.10.0.0/16"
    }
    ...

Nas chamadas é comum utilizar interpolações com ternarios para que parametros tambem seja possiveis via CLI durante as execuções, dessa forma a integração com um CD fica muito mais simplificada, lembrando que esta é apenas uma das organizações possiveis, nao se prenda a esta estrutura caso seu use-case seja outro!