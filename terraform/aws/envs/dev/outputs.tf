output "client_name" {
  value = "viavarejo"
}

output "environment" {
  value = "dev"
}

output "app_name" {
  value = "app"
}

output "app_params" {
  value = ""
}

output "instance_type" {
  value = "t2.micro"
}

output "app_type" {
  value = "app"
}

output "public_ip" {
  value = "false"
}

output "attach_eip" {
  value = "false"
}

output "allow_ssh" {
  value = "false"
}

output "region" {
  value = "us-east-1"
}

output "vpc_name" {
  value = "vpc-prd"
}

output "cidr_vpc" {
  value = "10.192.132.0/23"
}

# + EC2 Module

output "key_pair" {
  value = "palacios"
}

output "default_security_groups" {
  value = []
}
