variable "app_name" {
  default = "rundeck"
}

variable "app_type" {
  default = ""
}

variable "windows_username" {
  default = ""
}

variable "windows_password" {
  default = ""
}

variable "distro_type" {
  default = "linux"
}

variable "app_params" {
  default = ""
}

variable "region" {
  default = ""
}

variable "instance_type" {
  default = "t2.micro"
}

variable "public_ip" {
  default = false
}

variable "attach_eip" {
  default = true
}

variable "allow_rundeck" {
  default = true
}

variable "allow_ssh" {
  default = true
}

variable "allow_vpn" {
  default = false
}

variable "allow_dns" {
  default = false
}

variable "allow_http" {
  default = true
}

variable "allow_https" {
  default = false
}

variable "client_name" {
  default = "viavarejo"
}

variable "ami" {
  default = "ami-0922553b7b0369273"
}

variable "zone_id" {
  default = "Z23U5KXI0592KB"
}

variable "vpc_id" {
  default = "vpc-a7aaa3de"
}

variable "subnet_id" {
  default = "subnet-3a14fb71"
}

variable "environment" {
  default = "dev"
}

variable "key_name" {
  default = "palacios"
}

variable "owner" {
  default = "palacios"
}
