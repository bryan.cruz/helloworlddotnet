provider "aws" {
  region  = "${var.region == "" ?  module.environment.region : var.region}"
  profile = "rivendel-testes"
}

terraform {
  backend "s3" {
   bucket = "rivendel-testes-tf"
   key    = "services/APP_TEMPLATE"
   region = "us-east-1"
   profile = "rivendel-testes"
  }
}

module "environment" {
  source = "../../"
}

module "ec2" {
  source                  = "../../../../modules/ec2"
  app_name                = "${var.app_name == "" ?  module.environment.app_name : var.app_name}"
  environment             = "${module.environment.environment}"
  instance_type           = "${var.instance_type == "" ?  module.environment.instance_type : var.instance_type}"
  key_pair                = "${var.key_name}"
  subnet_id               = "${var.subnet_id}"
  vpc_id                  = "${var.vpc_id}"
  default_security_groups = ["${module.environment.default_security_groups}"]
  public_ip               = "${var.public_ip ? var.public_ip : module.environment.public_ip}"
  attach_eip              = "${var.attach_eip ? var.attach_eip : module.environment.attach_eip}"
  allow_ssh               = "${var.allow_ssh}"
  allow_dns               = "${var.allow_dns}"
  allow_vpn               = "${var.allow_vpn}"
  allow_http              = "${var.allow_http}"
  allow_https             = "${var.allow_https}"
  allow_rundeck           = "${var.allow_rundeck}"
  allow_dotnet            = "${var.allow_dotnet}"
  allow_nodejs            = "${var.allow_nodejs}"
  owner                   = "${var.owner}"
  ami_type                = "${var.distro_type}"
  ami                     = "${var.ami}"
}

module "route53" {
  source = "../../../../modules/route53"

  dns_entry   = "${var.app_name == "" ?  module.environment.app_name : var.app_name}"
  environment = "${module.environment.environment}"
  zone_name   = "${var.client_name == "" ?  module.environment.client_name : var.client_name}"
  zone_id     = "${var.zone_id}"
  ip_address  = "${module.ec2.public_ip}"
}
