variable "app_name" {}
variable "client_name" {
  default = "viavarejo"
}
variable "subnet_id" {}
variable "vpc_id" {}
variable "key_pair" {}

variable "instance_type" {
  default = "t2.small"
}

variable "environment" {
  default = "dev"
}

variable "user_data_file" {
  default = "user_data.tpl"
}

variable "allow_vpn" {
  default = false
}

variable "allow_ssh" {
  default = false
}

variable "allow_http" {
  default = false
}

variable "allow_rundeck" {
  default = false
}

variable "allow_https" {
  default = false
}

variable "allow_dns" {
  default = false
}

variable "allow_dotnet" {
  default = false
}

variable "allow_nodejs" {
  default = false
}

variable "public_ip" {
  default = false
}

variable "attach_eip" {
  default = false
}

variable "ami_type" {
  default = "linux"
}

variable "ami" {
  default = "ami-0922553b7b0369273"
}

variable "region" {
  default = "us-east-1"
}

variable "backup" {
  default = "yes"
}

variable "number_of_instances" {
  default = 1
}

variable "default_security_groups" {
  type = "list"
}

variable "owner" {
  default = ""
}
